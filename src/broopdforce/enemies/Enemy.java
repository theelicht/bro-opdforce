package broopdforce.enemies;

import java.util.List;
import java.util.Random;
import broopdforce.main.Broopdforce;
import broopdforce.main.IMovement;
import broopdforce.main.Timer;
import broopdforce.player.Player;
import broopdforce.projectile.Bullet;
import nl.han.ica.oopg.collision.ICollidableWithGameObjects;
import nl.han.ica.oopg.objects.GameObject;
import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.objects.SpriteObject;

public abstract class Enemy extends SpriteObject implements IMovement, ICollidableWithGameObjects {
	protected float movementSpeed;
	protected Sprite sprite;
	protected int randomDirection;
	protected float originX;
	protected float originY;
	protected Player player;
	protected Broopdforce world;
	protected Timer movementTimer;
	protected Timer shootingTimer;

	private Random r = new Random();

	// Enemy constructor creates a new spriteobject in the game engine
	public Enemy(Sprite sprite, float movementSpeed, float originX, float originY, Broopdforce world, Player player) {
		super(sprite);
		this.movementSpeed = movementSpeed;
		this.originX = originX;
		this.originY = originY;
		this.world = world;

		// Create 2 new timers for movement and shooting
		this.movementTimer = new Timer();
		this.shootingTimer = new Timer();
	}

	// Movement updates the location of an enemy
	public void movement() {
		int interval = 1000;
		// Function to keep enemy near the point where it was placed
		keepEnemyNearPoint();
		if (movementTimer.timerFinished(interval)) {
			randomDirection = r.nextInt(2);
			if (randomDirection == 1) {
				moveLeft();
			} else {
				moveRight();
			}
		}
	}

	// Keeps the enemy from going too far from the point it was placed
	private void keepEnemyNearPoint() {
		int maxOffset = 64;
		
		if (getX() > originX + maxOffset) {
			setX(originX + maxOffset);
		} else if (getX() < originX - maxOffset) {
			setX(originX - maxOffset);
		}
	}

	public int getBulletDirection() {
		int bulletDirection;
		
		// If: the player is to the left shoot left
		if (player.getX() < getX()) {
			bulletDirection = 270;
			// Else: shoot right
		} else {
			bulletDirection = 90;
		}

		return bulletDirection;
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> gameObjects) {
		for (GameObject object : gameObjects) {
			if (object instanceof Bullet) {
				// When a collision occurs delete both the enemy and the bullet
				world.deleteGameObject(object);
				world.deleteGameObject(this);
			}
		}
	}

	// This function puts a delay on shooting with the given interval
	public void shootWithDelay(int interval) {
		if (shootingTimer.timerFinished(interval)) {
			shoot();
		}
	}

	@Override
	public void moveLeft() {
		int direction = 270;
		setDirectionSpeed(direction, movementSpeed);
	}

	@Override
	public void moveRight() {
		int direction = 90;
		setDirectionSpeed(direction, movementSpeed);
	}

	@Override
	public void moveUp() {
	}

	@Override
	public void moveDown() {
	}

	public abstract void shoot();

}
