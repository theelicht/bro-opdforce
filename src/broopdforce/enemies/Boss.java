package broopdforce.enemies;

import broopdforce.main.Broopdforce;
import broopdforce.player.Player;
import broopdforce.projectile.Bullet;
import nl.han.ica.oopg.objects.Sprite;

public class Boss extends Enemy {
	private Bullet bullet;
	private Sprite bulletSprite;
	private Broopdforce world;

	public Boss(Sprite sprite, Sprite bulletSprite, float movementSpeed, float originX, float originY,
			Player player, Broopdforce world) {
		super(sprite, movementSpeed, originX, originY, world, player);
		this.bulletSprite = bulletSprite;
		this.world = world;
		this.player = player;
	}

	@Override
	public void update() {
		movement();
		shootWithDelay(750);
	}

	@Override
	// Mini Boss and Boss shoot function
	public void shoot() {
		int bulletDirection = getBulletDirection();
		float bulletX = 0;
		float bulletY = getY();
		
		// Puts the xPos of Bullet on the right side of enemy
		if (bulletDirection == 90) {
			bulletX = getX() + getWidth();
		} else if (bulletDirection == 270) {
			bulletX = getX() - getWidth();
		}

		// Creates a bullet
		bullet = new Bullet(bulletDirection, bulletSprite, world, bulletX, bulletY);
		// Fires the bullet
		bullet.useProjectile();
	}
}
