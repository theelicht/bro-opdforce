package broopdforce.player;

import java.util.ArrayList;
import java.util.List;
import broopdforce.main.Broopdforce;
import broopdforce.main.IMovement;
import broopdforce.menu.PauseMenu;
import broopdforce.projectile.Bullet;
import broopdforce.world.CollidableTile;
import nl.han.ica.oopg.collision.CollidedTile;
import nl.han.ica.oopg.collision.CollisionSide;
import nl.han.ica.oopg.collision.ICollidableWithGameObjects;
import nl.han.ica.oopg.collision.ICollidableWithTiles;
import nl.han.ica.oopg.dashboard.Dashboard;
import nl.han.ica.oopg.dashboard.FPSCounter;
import nl.han.ica.oopg.exceptions.TileNotFoundException;
import nl.han.ica.oopg.objects.GameObject;
import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.objects.SpriteObject;
import nl.han.ica.oopg.objects.TextObject;
import nl.han.ica.oopg.userinput.IKeyInput;
import processing.core.PConstants;
import processing.core.PVector;

public class Player extends SpriteObject
		implements IMovement, ICollidableWithTiles, IKeyInput, ICollidableWithGameObjects {
	private float xPos, yPos;
	private int lives;
	private Broopdforce world;
	private Sprite bulletSprite;
	private PauseMenu pauseMenu;
	private Dashboard hud;
	TextObject amountOfLives;
	private boolean pauseMenuOpen = false;
	private boolean isGameOver = false;
	private int movementSpeed = 5;
	char characters[] = { 'w', 'a', 's', 'd', ' ' };
	private ArrayList<Key> keys = new ArrayList<>();

	public Player(Sprite playerSprite, Sprite bulletSprite, float xPos, float yPos, Broopdforce world) {
		super(playerSprite);
		this.bulletSprite = bulletSprite;
		this.xPos = xPos;
		this.yPos = yPos;
		this.world = world;
		this.lives = 3;
		createHud();
		setGravity((float) 0.5);
		createKeyArray();
	}

	//Checks for tile collisions with the game map
	@Override
	public void tileCollisionOccurred(List<CollidedTile> collidedTiles) {
		PVector vector;

		for (CollidedTile ct : collidedTiles) {
			if (ct.getTile() instanceof CollidableTile) {
				if (CollisionSide.TOP.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y - getHeight());
						resetPlayerSpeed();
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
				if (CollisionSide.RIGHT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x + (getWidth() * 2));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
				if (CollisionSide.BOTTOM.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y + getHeight());
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
				if (CollisionSide.LEFT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x - getWidth());
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}

	}

	//Resets the player speed when colliding on the top of the tile
	//This method is needed to prevent glitches through the map due to gravity
	private void resetPlayerSpeed() {
		setDirectionSpeed(0, 0);
	}

	//Creates an array of keys to use in movement
	private void createKeyArray() {
		for (int i = 0; i < characters.length; i++) {
			Key Key = new Key(characters[i]);
			keys.add(Key);
		}
	}

	//Checks if the player got hit by a bullet
	@Override
	public void gameObjectCollisionOccurred(List<GameObject> gameObjects) {
		for (GameObject object : gameObjects) {
			if (object instanceof Bullet) {
				lives--;
				updateLives();
				setX(xPos);
				setY(yPos);
				world.deleteGameObject(object);
			}
		}
	}

	//A function is called based on what keys are pressed
	private void movement() {
		for (Key key : keys) {
			if (key.isKeyPressed()) {
				if (!isGameOver) {
					switch (key.getKey()) {
					case 'w':
						moveUp();
						break;
					case 'a':
						moveLeft();
						break;
					case 'd':
						moveRight();
						break;
					case 's':
						moveDown();
						break;
					case ' ':
						shoot();
						break;
					}

				}
			}
		}
	}

	//Movement functions 
	@Override
	public void moveLeft() {
		int direction = 270;
		setDirectionSpeed(direction, movementSpeed);
	}

	@Override
	public void moveRight() {
		int direction = 90;
		setDirectionSpeed(direction, movementSpeed);
	}

	@Override
	public void moveUp() {
		int direction = 0;
		int jumpSpeed = 15;
		setDirectionSpeed(direction, jumpSpeed);
	}

	@Override
	public void moveDown() {
		int direction = 180;
		setDirectionSpeed(direction, movementSpeed);
	}

	//Handles the actual key inputs
	@Override
	public void keyPressed(int arg0, char arg1) {
		//if a key is pressed that keyPressed is set to true
		for (Key key : keys) {
			if (key.getKey() == arg1) {
				key.setKeyPressed(true);
			}
		}
		if (arg1 == 'p') {
			drawPauseMenu();
		}
	}

	//Checks for keys that have been released
	@Override
	public void keyReleased(int arg0, char arg1) {
		if (arg1 != ' ') {
			setDirectionSpeed(0, 0);
		}
		//if a key is released that keyPressed is set to false
		for (Key key : keys) {
			if (key.getKey() == arg1) {
				key.setKeyPressed(false);
			}
		}
	}

	@Override
	public void update() {
		//If lives reaches 0 the game ends
		if (lives <= 0) {
			lives++;
			isGameOver = true;
			gameOver();
		}
		movement();
	}

	//Function to decide where the bullet goes, based on the direction the player is moving
	private int getBulletDirection() {
		int left = 270;
		int right = 90;
		if (getDirection() == left) {
			return left;
		} else {
			return right;
		}
	}

	//Shoot function shoots a bullet from the player location
	private void shoot() {
		int left = 270;
		float playerX;
		float playerY = getY();
		int bulletDirection = getBulletDirection();
		if (bulletDirection == left) {
			playerX = getX() - getWidth();
		} else {
			playerX = getX() + getWidth();
		}
		// Creates a bullet
		Bullet bullet = new Bullet(bulletDirection, bulletSprite, world, playerX, playerY);
		// Fires the bullet
		bullet.useProjectile();
	}

	//If lives reaches 0 gameover prints a message
	private void gameOver() {
		int textSize = 45;
		int offset = textSize * 8;
		if (isGameOver) {
			world.textAlign(PConstants.CENTER);
			TextObject gameOver = new TextObject("Game Over", textSize);
			world.addGameObject(gameOver, getX(), getY() - offset);
			TextObject openPause = new TextObject("Press P to open pause Menu", textSize);
			world.addGameObject(openPause, getX(), getY() - offset + textSize);
			isGameOver = false;
		}
	}

	//Draws a pause menu when pauseMenuOpen is false
	private void drawPauseMenu() {
		if (!pauseMenuOpen) {
			pauseMenu = new PauseMenu(world);
			world.addGameObject(pauseMenu);
			pauseMenuOpen = !pauseMenuOpen;
			world.pauseGame();
		} else {
			world.deleteGameObject(pauseMenu);
			pauseMenuOpen = !pauseMenuOpen;
			world.resumeGame();
		}
	}

	public float getPlayerX() {
		return xPos;
	}

	public float getPlayerY() {
		return yPos;
	}

	//Draws a hud with a framecounter and the amount of lives
	private void createHud() {
		int textSize = 45;
		int livesX = textSize;
		int livesY = world.displayHeight - textSize;
		hud = new Dashboard(getX(), getY(), world.displayWidth, world.displayHeight);
		amountOfLives = new TextObject("Lives: " + lives, textSize);
		FPSCounter fps = new FPSCounter(livesX, livesY + textSize);
		hud.addGameObject(amountOfLives, livesX, livesY);
		hud.addGameObject(fps);
		world.addDashboard(hud);
	}

	//updates the amount of lives on the dashboard
	private void updateLives() {
		amountOfLives.setText("Lives: " + lives);
	}
}
