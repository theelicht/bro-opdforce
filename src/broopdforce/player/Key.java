package broopdforce.player;


public class Key {
	private char key;
	private boolean keyPressed = false;
	
	public Key(char key) {
		this.key = key;
	}
	
	public boolean isKeyPressed() {
		return keyPressed;
	}
	
	public char getKey() {
		return key;
	}
	
	public void setKey(char key) {
		this.key = key;
	}
	
	public void setKeyPressed(boolean state) {
		keyPressed = state;
	}
	
}
