package broopdforce.menu;

import broopdforce.main.Broopdforce;
import processing.core.PConstants;

public class PauseMenu extends Menu {
	private Button exit;
	private int xPos;
	private int yPos;
	private int offset = 100;

	public PauseMenu(Broopdforce world) {
		super(world);
	}

	//Draws the pauseMenu
	public void drawMenu() {
		drawAccessoires();
		exit.drawButton();
	}

	//Draws a simple red box behind the button
	public void drawAccessoires() {
		// Background
		int roundedEdge = 20;
		world.fill(255, 0, 0);
		world.rect(xPos, yPos, xPos + offset, yPos + offset/2,
				roundedEdge);
	}

	//When enter is pressed in the pause menu the game exits
	@Override
	public void keyPressed(int arg0, char arg1) {
		switch (world.keyCode) {
		case PConstants.ENTER:
			spelGestart = true;
			System.exit(0);
			exit.setButtonPressed(true);
			break;
		}
	}

	//Creates the instance of the button used
	@Override
	public void initializeButtons() {
		yPos = 100;
		xPos = 200;
		int width = 250;
		exit = new Button(xPos, yPos, width, "Exit Game", world);
	}

}
