package broopdforce.menu;

import broopdforce.main.Broopdforce;
import nl.han.ica.oopg.objects.GameObject;
import processing.core.PConstants;
import processing.core.PGraphics;

public class Button extends GameObject {
	private int yPos;
	private int xPos;
	private Broopdforce world;
	private int buttonWidth;
	private int buttonHeight;
	private boolean buttonPressed = false;
	private boolean buttonSelected = false;
	private String string;

	public Button(int yPos, String string, Broopdforce world) {
		this.world = world;
		this.yPos = yPos;
		this.string = string;
		buttonWidth = world.displayWidth / 2;
		xPos = world.displayWidth / 2;
	}

	//Second constructor for when a non centered xPos is needed
	public Button(int xPos, int yPos, int width, String string, Broopdforce world) {
		this.world = world;
		this.yPos = yPos;
		this.string = string;
		this.buttonWidth = width;
		this.xPos = xPos;
	}

	//Draws the entire button, including text
	public void drawButton() {
		buttonHeight = world.displayHeight / 8;
		// Draws a button
		world.rectMode(PConstants.CENTER);
		if (buttonSelected) {
			world.fill(50);
			System.out.println(string);
		} else {
			world.fill(0);
		}
		int roundedEdge = 20;
		world.rect(xPos, yPos, buttonWidth, buttonHeight, roundedEdge);
		world.fill(255);
		world.textSize(50);
		world.textAlign(PConstants.CENTER, PConstants.CENTER);
		world.text(string, xPos, yPos);
	}

	public boolean isButtonPressed() {
		return buttonPressed;
	}

	public void setButtonPressed(boolean buttonPressed) {
		this.buttonPressed = buttonPressed;
	}

	public boolean isButtonSelected() {
		return buttonSelected;
	}

	public void setButtonSelected(boolean selection) {
		this.buttonSelected = selection;
	}

	@Override
	public void draw(PGraphics arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}
}
