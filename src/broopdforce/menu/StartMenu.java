package broopdforce.menu;

import broopdforce.main.Broopdforce;
import processing.core.PConstants;
import processing.core.PImage;

public class StartMenu extends Menu {
	private Button start;
	private Button nieuw;
	private PImage header;
	private int yPos;
	private int xPos;
	private int offset = 200;

	public StartMenu(PImage header, Broopdforce world) {
		super(world);
		this.header = header;
		world.pauseGame();
	}

	//Draws the startMenu, including logo and background
	public void drawMenu() {
		drawAccessoires();
		start.drawButton();
		nieuw.drawButton();
		world.image(header, xPos, yPos - offset);
	}

	//Draws the background and a box behind the logo to make it stand out
	public void drawAccessoires() {
		// Background
		PImage startBackground = world.loadImage(dataPath + "background.png");
		world.image(startBackground, xPos, yPos);
		// Rectangle behind Logo
		int roundedEdge = 20;
		world.fill(255, 0, 0);
		world.rect(xPos, yPos - offset, header.width + 50, header.height + 50, roundedEdge);
	}

	//Creates the instances of the buttons
	@Override
	public void initializeButtons() {
		int offset = 200;
		world.imageMode(PConstants.CENTER);
		yPos = world.displayHeight / 2;
		xPos = world.displayWidth / 2;
		start = new Button(yPos, "Continue Game", world);
		nieuw = new Button(yPos + offset, "Start new Game", world);
	}
	
	//Runs when enter is pressed to start the game based on the option picked
	public void enterGame() {
		spelGestart = true;
		world.resumeGame();
		if (start.isButtonSelected()) {
			start.setButtonPressed(true);
		} else {
			nieuw.setButtonPressed(true);
		}
	}

	//Handles the selection of the buttons and the enter press
	@Override
	public void keyPressed(int arg0, char arg1) {
		switch (world.keyCode) {
		case PConstants.UP:
			start.setButtonSelected(true);
			nieuw.setButtonSelected(false);
			break;
		case PConstants.DOWN:
			nieuw.setButtonSelected(true);
			start.setButtonSelected(false);
			break;
		case PConstants.ENTER:
			enterGame();
			break;
		}
	}

}
