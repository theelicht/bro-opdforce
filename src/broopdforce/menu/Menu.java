package broopdforce.menu;

import broopdforce.main.Broopdforce;
import nl.han.ica.oopg.objects.GameObject;
import nl.han.ica.oopg.userinput.IKeyInput;
import processing.core.PGraphics;

public abstract class Menu extends GameObject implements IKeyInput {
	protected Broopdforce world;
	protected boolean spelGestart = false;
	protected final String dataPath = "src/broopdforce/data/";

	public Menu(Broopdforce world) {
		super(0, 0, world.displayWidth, world.displayHeight);
		this.world = world;
		initializeButtons();

	}
	
	public boolean isSpelGestart() {
		return spelGestart;
	}

	public void setSpelGestart(boolean spelGestart) {
		this.spelGestart = spelGestart;
	}

	public abstract void drawMenu();

	public abstract void initializeButtons();

	@Override
	public abstract void keyPressed(int arg0, char arg1);

	@Override
	public void keyReleased(int arg0, char arg1) {
	}

	@Override
	public void draw(PGraphics arg0) {
		drawMenu();
	}

	@Override
	public void update() {
	}
}
