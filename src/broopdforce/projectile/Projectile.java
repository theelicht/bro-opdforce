package broopdforce.projectile;

import java.util.List;

import nl.han.ica.oopg.collision.ICollidableWithGameObjects;
import nl.han.ica.oopg.objects.GameObject;
import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.objects.SpriteObject;

public abstract class Projectile extends SpriteObject implements ICollidableWithGameObjects {

	public Projectile(Sprite sprite) {
		super(sprite);
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> arg0) {
		// TODO Auto-generated method stub

	}

	public abstract void useProjectile();

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

}
