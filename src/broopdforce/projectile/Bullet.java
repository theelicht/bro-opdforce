package broopdforce.projectile;

import java.util.List;

import broopdforce.main.Broopdforce;
import broopdforce.world.CollidableTile;
import nl.han.ica.oopg.collision.CollidedTile;
import nl.han.ica.oopg.collision.CollisionSide;
import nl.han.ica.oopg.collision.ICollidableWithTiles;
import nl.han.ica.oopg.exceptions.TileNotFoundException;
import nl.han.ica.oopg.objects.Sprite;

public class Bullet extends Projectile implements ICollidableWithTiles {
	private int movementSpeed = 7;
	private int direction;
	private Broopdforce world; 
	
 	public Bullet(int direction, Sprite sprite, Broopdforce world, float enemyX, float enemyY) {
 		super(sprite);
 		this.direction = direction;
 		this.world = world;
 		world.addGameObject(this, enemyX, enemyY);
 	}

 	//Uses the projectile, sets a direction and speed it moves in
	@Override
	public void useProjectile() {
		setDirectionSpeed(direction, movementSpeed);
	}
	
	//Deletes the bullet if it goes out of bounds
	@Override
	public void update() {
		if (this.x <= 0 || this.y <= 0 || this.x >= world.getWidth() || this.y >= world.getHeight()) {
			world.deleteGameObject(this);
		}
	}

	//Deletes the bullet if it collides with a world tile.
	@Override
	public void tileCollisionOccurred(List<CollidedTile> collidedTiles) {
		for (CollidedTile ct : collidedTiles) {
			if (ct.getTile() instanceof CollidableTile) {
				if (CollisionSide.RIGHT.equals(ct.getCollisionSide())) {
					try {
						world.deleteGameObject(this);
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
				if (CollisionSide.LEFT.equals(ct.getCollisionSide())) {
					try {
						world.deleteGameObject(this);
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}

}
