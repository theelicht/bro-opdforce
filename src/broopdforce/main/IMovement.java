package broopdforce.main;

public interface IMovement {
	
	public abstract void moveLeft();
	
	public abstract void moveRight();
	
	public abstract void moveUp();
	
	public abstract void moveDown();
	
}