package broopdforce.main;

public class Timer {
	private long startTime;
	
	public Timer() {
		startTime = System.currentTimeMillis();
	}
	
	public boolean timerFinished(int interval) {
		if (System.currentTimeMillis() - startTime >= interval) {
			resetTimer();
			return true;
		} else {
			return false;
		}
	}
	
	private void resetTimer() {
		startTime = System.currentTimeMillis();
	}
}
